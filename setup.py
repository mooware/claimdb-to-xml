from distutils.core import setup
import py2exe

setup(
    console=['claimdb-to-xml.py'],
    zipfile=None,
    options={
        'py2exe': {
            'bundle_files': 1,
            'compressed': True,
            'dll_excludes': ['w9xpopen.exe'],
            'includes': ['decimal'] # for pyodbc
        }
    }
)
