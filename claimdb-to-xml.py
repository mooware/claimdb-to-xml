import sys, os.path

# set the path to find the correct mapping module
# when using py2exe and a separate mapping.py
sys.path.append(os.path.dirname(os.path.abspath(sys.argv[0])))
import mapping

# i use MDBTools on Linux
import pyodbc, platform
if platform.system() == 'Windows':
    ODBC_DRIVER = '{Microsoft Access Driver (*.mdb)}'
else:
    ODBC_DRIVER = 'MDBTools'

def connect_to_db(filename):
    connstr = 'DRIVER={0};DBQ={1}'.format(ODBC_DRIVER, filename)
    conn = pyodbc.connect(connstr)
    if conn:
        return conn

def read_db(filename):
    result = []
    conn = connect_to_db(filename)
    try:
        cur = conn.cursor()
        # fetch tickets
        cur.execute('SELECT * FROM tbl_GIG01_ClaimFile')
        tickets = cur.fetchall()
        ticketcols = [c[0] for c in cur.description]

        # fetching parts for every ticket separately takes way too long,
        # so read all and then filter locally
        cur.execute('SELECT * FROM tbl_GIG01_ClaimFile_Parts')
        parts = cur.fetchall()
        partcols = [c[0] for c in cur.description]

        tickets_joinidx = ticketcols.index('ClaimID')
        parts_joinidx = partcols.index('ClaimID')
        for t in tickets:
            ticket_parts = [p for p in parts if t[tickets_joinidx] == p[parts_joinidx]]
            result.append((t, ticket_parts))

        return (result, ticketcols, partcols)
    finally:
        conn.close()

def make_xml(data):
    import io
    import util
    # import both for py2exe
    import xml.etree.ElementTree
    import xml.etree.cElementTree as et
    # first build the xml structure
    root_tag = et.Element('ReportingFile')
    root_tag.set('xmlns', 'http://schemas.gigaset.com/CS/2012/SePPReportingFile')

    (tickets, ticketcols, partcols) = data
    for (ticket, parts) in tickets:
        ticket_tag = et.SubElement(root_tag, 'Ticket')
        # attributes
        for (dbname, xmlname, convert) in mapping.TICKET_ATTRIBUTES:
            value = ticket[ticketcols.index(dbname)]
            if util.use_db_value(value):
                ticket_tag.set(xmlname, convert(value))
        # elements
        for (dbname, xmlname, convert) in mapping.TICKET_ELEMENTS:
            value = ticket[ticketcols.index(dbname)]
            if util.use_db_value(value):
                subel = et.SubElement(ticket_tag, xmlname)
                subel.text = convert(value)
        # materials
        materials_tag = et.SubElement(ticket_tag, 'Materials')
        for part in parts:
            material_tag = et.SubElement(materials_tag, 'Material')
            for (dbname, xmlname, convert) in mapping.MATERIAL_ATTRIBUTES:
                value = part[partcols.index(dbname)]
                if util.use_db_value(value):
                    material_tag.set(xmlname, convert(value))
    # prepare the xml output
    util.indent_xml(root_tag)
    doc = et.ElementTree(root_tag)
    # ElementTree has encoding issues,
    # so use a local buffer and encode separately
    buf = io.BytesIO()
    doc.write(buf, encoding='utf-8', xml_declaration=True)
    s = buf.getvalue().decode('utf-8')
    # fix encoding in xml declaration
    s = s.replace('utf-8', 'utf-16', 1)
    s = s.encode('utf-16')
    return s

def main():
    if len(sys.argv) < 2:
        progname = os.path.basename(sys.argv[0])
        print("usage: {0} <mdb file> [<xml result file>]".format(progname))
        return

    infile = sys.argv[1]
    if len(sys.argv) < 3:
        (s, ext) = os.path.splitext(infile)
        outfile = s + ".xml"
    else:
        outfile = sys.argv[2]

    try:
        print("reading input file {0}".format(infile))
        data = read_db(infile)
        print("read {0} tickets".format(len(data[0])))

        print("generating xml document")
        doc = make_xml(data)

        print("writing result file {0}".format(outfile))
        try:
            with open(outfile, 'wb') as f:
                f.write(doc)
        except:
            # in case of error, delete the file
            os.remove(outfile)
            raise

        print("done")
    except:
        print("error occurred, abort")
        import traceback
        print(traceback.format_exc())

    print("press enter to exit ...")
    sys.stdin.readline()

if __name__ == "__main__":
    main()
