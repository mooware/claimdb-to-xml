from util import boolconv, dateconv, strconv, upstrconv, makeformatconv

# format for these lists: (db-name, xml-name, convert-function)

# mapping from db columns to ticket attributes
TICKET_ATTRIBUTES = [
    ('ID LSP'   , 'LSP', makeformatconv('{:0>10}')),
    ('RefNo LSP', 'LSPRefNo', strconv),
    ('RefNo CCC', 'RefNoCCC', strconv)
]

# mapping from db columns to ticket elements
TICKET_ELEMENTS = [
    ('ProdModel'   , 'ProductModel', strconv),
    ('SysReturn'   , 'SysReturn', boolconv),
    ('PartNoComp'  , 'PartnumberComponent', strconv),
    ('SerialNo'    , 'SerialNo', strconv),
    ('SerialNoNew' , 'SerialNoNew', strconv),
    ('DateCode'    , 'DateCode', upstrconv),
    ('DatePurch'   , 'DatePurchase', dateconv),
    ('DateIn'      , 'DateIn', dateconv),
    ('DateRep'     , 'DateRep', dateconv),
    ('DateOut'     , 'DateOut', dateconv),
    ('TransCode'   , 'TransportationCode', upstrconv),
    ('ClientCode'  , 'ClientCode', strconv),
    ('DiagCode'    , 'DiagnoseCode', strconv),
    ('Sporadic'    , 'Sporadic', boolconv),
    ('WarrClaim'   , 'WarrantyClaim', upstrconv),
    ('WarrConf'    , 'WarrantyConfirm', upstrconv),
    ('FinalRepStat', 'FinalRepairStatus', upstrconv),
    ('Remark'      , 'Remark', strconv)
]

# mapping from db columns to material attributes
MATERIAL_ATTRIBUTES = [
    ('PartNoMat', 'Partnumber', strconv),
    ('FailCause', 'FailCause', boolconv)
]
