# helper to indent the xml, from http://effbot.org/zone/element-lib.htm
def indent_xml(elem, levels=0):
    import os
    ilevel = ' ' * 2
    i = os.linesep + (levels * ilevel)
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + ilevel
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for child in elem:
            indent_xml(child, levels + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if levels and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

# determine whether an xml element should be created
def use_db_value(value):
    # never use None
    if value is None:
        return False
    # no empty strings
    if isinstance(value, unicode) or isinstance(value, str):
        return len(value.strip()) != 0
    # the rest is fine
    return True

def boolconv(value):
    # input format: 'Y' / 'N'
    if value.upper() == 'Y':
        return 'true'
    else:
        return 'false'

def dateconv(value):
    # input format: 'yyyymmdd'
    year = value[0:4]
    month = value[4:6]
    day = value[6:8]
    return '{0}-{1}-{2}'.format(year, month, day)

def strconv(value):
    if isinstance(value, unicode):
        # no conversion necessary
        return value.strip()
    # i assume this is the encoding in the db
    enc = 'latin1'
    try:
        return unicode(value, enc, 'strict').strip()
    except UnicodeEncodeError as e:
        print("warning, invalid string encountered: '{0}'".format(value))
        print(str(e))
        # replace with unicode symbol for "invalid"
        return unicode(value, enc, 'replace').strip()

def upstrconv(value):
    return strconv(value).upper()

def makeformatconv(formatstring):
    def formatconv(value):
        ufmt = strconv(formatstring)
        uval = strconv(value)
        return ufmt.format(uval)
    return formatconv
